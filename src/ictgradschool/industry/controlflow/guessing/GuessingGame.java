package ictgradschool.industry.controlflow.guessing;

import ictgradschool.Keyboard;

import javax.swing.plaf.basic.BasicSplitPaneUI;

/**
 * A guessing game!
 */
public class GuessingGame
    {

    public void start()
        {
        // TODO Write your code here.
        int goal = getARandomNumber();
        letsGuess(goal);
        }

    public int getARandomNumber()
        {
        return ((int) (Math.random() * (100 - 1)) + 1);
        }

    public void letsGuess(int goal)
        {
            int guess = 0;
            while (guess != goal)
            {
                System.out.print("Enter your guess(1-100): ");
                guess = Integer.parseInt(Keyboard.readInput());
                if(guess > goal)
                {
                    System.out.println("Too high, try again!");
                }
                else if(guess < goal)
                {
                    System.out.println("Too low, try again!");
                }
                else
                {
                    System.out.println("Perfect!");
                    System.out.println("Goodbye");
                }
            }
        }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args)
        {

        GuessingGame ex = new GuessingGame();
        ex.start();

        }
    }
