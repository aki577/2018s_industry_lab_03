package ictgradschool.industry.controlflow.rockpaperscissors;

import ictgradschool.Keyboard;

import java.security.Key;

/**
 * A game of Rock, Paper Scissors
 */
public class RockPaperScissors
    {

    public static final int ROCK = 1;
    public static final int PAPER = 2;
    public static final int SCISSORS = 3;

    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public void start()
        {
        String name;
        int choice = 0;
        int Cchoice;
        System.out.print("Hi! What's your name? ");
        name = Keyboard.readInput();
        while (true) {
            System.out.println();
            System.out.println("1. Rock");
            System.out.println("2. Paper");
            System.out.println("3. Scissors");
            System.out.println("4. Quit");
            System.out.print("Enter choice: ");
            choice = Integer.parseInt(Keyboard.readInput());
            if (choice == 4) {
                break;
            }
            System.out.println();
            displayPlayerChoice(name, choice);
            Cchoice = getComputerChoice();
            displayPlayerChoice("Computer", Cchoice);
            if (draw(choice, Cchoice)) {
                System.out.print(getResultString(choice, Cchoice));
            } else if (userWins(choice, Cchoice)) {
                System.out.print(name + " wins because " + getResultString(choice, Cchoice));
            } else {
                System.out.print("Computer" + " wins because " + getResultString(choice, Cchoice));
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("Goodbye " + name + ". Thanks for playing :)");
        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        }


    public void displayPlayerChoice(String name, int choice)
        {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
        String Schoice = "";
        switch (choice) {
            case 1:
                Schoice = "rock.";
                break;
            case 2:
                Schoice = "paper.";
                break;
            case 3:
                Schoice = "scissors.";
        }
        System.out.println(name + " chose " + Schoice);
        }

    public int getComputerChoice()
        {
        return (int) (Math.random() * 3 + 1);
        }

    public boolean draw(int playerChoice, int computerChoice)
        {
        if (playerChoice == computerChoice)
            return true;
        else return false;
        }

    public boolean userWins(int playerChoice, int computerChoice)
        {
        // TODO Determine who wins and return true if the player won, false otherwise.
        switch (playerChoice) {
            case 1:
                if (computerChoice == 3) {
                    return true;
                }
                break;
            case 2:
                if (computerChoice == 1) {
                    return true;
                }
                break;
            case 3:
                if (computerChoice == 2) {
                    return true;
                }
                break;
            default:
                return false;
        }
        return false;
        }

    public String getResultString(int playerChoice, int computerChoice)
        {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = "you chose the same as the computer";
        if (playerChoice == computerChoice) {
            return TIE;
        }
        if (playerChoice == 1 || computerChoice == 1) {
            if (playerChoice == 3 || computerChoice == 3) {
                return ROCK_WINS;
            }
        }
        if (playerChoice == 2 || computerChoice == 2) {
            if (playerChoice == 1 || computerChoice == 1) {
                return PAPER_WINS;
            }
        }
        if (playerChoice == 3 || computerChoice == 3) {
            if (playerChoice == 2 || computerChoice == 2) {
                return SCISSORS_WINS;
            }
        }
        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.

        return null;
        }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args)
        {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

        }
    }
